import React, { useState } from "react";
import { Route, Routes } from "react-router-dom";
import { Homepage } from "../pages/Homepage";
import ButtonAppBar from "../assets/components/ButtonAppBar";
import { AboutPage } from "../pages/AboutPage";
import { HobbyPage } from "../pages/HobbyPage";
import { ViewPage } from "../pages/ViewPage";

export const App = () => {
  const [Data, setData] = useState([]);
  const addData = (newList) => {
    setData(newList);
  };
  //   console.log(Data);
  return (
    <div>
      <ButtonAppBar></ButtonAppBar>
      <Routes>
        <Route path="/" element={<Homepage dataUpdate={addData} />} />
        <Route path="/view" element={<ViewPage />} />
        <Route path="/about" element={<AboutPage />} />
        <Route path="/hobby" element={<HobbyPage value={Data} />} />
      </Routes>
    </div>
  );
};
