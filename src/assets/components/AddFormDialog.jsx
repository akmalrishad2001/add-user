import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { useState,useEffect} from "react";
import { useNavigate } from "react-router-dom";

export default function AddFormDialog( { List, onListUpdate}) {
  const navigate = useNavigate()

  const [open, setOpen] = useState(false);

  
  const [Id, setId] = useState(0);
  const [Name, setName] = useState("");
  const [Address, setAddress] = useState("");
  const [Hobby, setHobby] = useState("");
  
  const [nameError, setNameError] = useState("");
  const [addressError, setAddressError] = useState("");
  const [hobbyError, setHobbyError] = useState("");


  const handleClickOpen = () => {
    setOpen(true)
    setName("")
    setAddress("")
    setHobby("")
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSave = (e) => {
    e.preventDefault();
    if (!Name || !Name.length) {
      setNameError("nama harus diisi");
      return false;
    }
    if (!Address || !Address.length) {
      setAddressError("address harus diisi");
      return false;
    }
    if (!Hobby || !Hobby.length) {
      setHobbyError("hobby harus diisi");
      return false;
    }
    
    // console.log(Name, Address, Hobby);
    if(List.length === 0){
      setId(1)
    }else{
      setId(List.length + 1)
    }
    
    const data = { Id,Name, Address, Hobby};

    const newList = [...List, data];
    // navigate('/',{state:{id:Id,name:Name,address:Address,hobby:Hobby}})
    onListUpdate(newList)
    setOpen(false);
  };
  

  return (
    <div>
      <Button color="inherit" onClick={handleClickOpen}>
        ADD USER
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Add User</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="name"
            type="text"
            fullWidth
            variant="standard"
            value={Name}
            error={nameError && nameError.length?true:false}
            helperText={nameError}
            onChange={(e) => setName(e.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="address"
            label="address"
            type="text"
            fullWidth
            variant="standard"
            value={Address}
            error={addressError && addressError.length?true:false}
            helperText={addressError}
            onChange={(e) => setAddress(e.target.value)}
          />
          <TextField
            autoFocus
            margin="dense"
            id="hobby"
            label="hobby"
            type="text"
            fullWidth
            variant="standard"
            value={Hobby}
            error={hobbyError && hobbyError.length?true:false}
            helperText={hobbyError}
            onChange={(e) => setHobby(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSave}>save</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
