import React from "react";
// import "../assets/css/global.css";

export const HobbyPage = (props) => {
  //   console.log(props?.value);
  return (
    <div className="hobby">
      <h1>List Hobby saat ini</h1>
      {props?.value.length !== 0 ? (
        props?.value.map((a) => {
          return <div>{a.Hobby}</div>;
        })
      ) : (
        <div>0 hobby</div>
      )}
    </div>
  );
};
