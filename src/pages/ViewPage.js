import React from "react";
import { useLocation } from "react-router-dom";

export const ViewPage = () => {
  const dataState = useLocation();
  //   console.log(dataState);
  return (
    <div className="view">
      <h1>Name: {dataState.state.name}</h1>
      <h1>Address: {dataState.state.address}</h1>
      <h1>Hobby: {dataState.state.hobby}</h1>
    </div>
  );
};
