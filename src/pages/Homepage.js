import React, { useState } from "react";
import ButtonAppBar from "../assets/components/ButtonAppBar";
import "../assets/css/global.css";
import { Button, TextField } from "@mui/material";
import { Edit } from "@mui/icons-material";
import { EditFormDialog } from "../assets/components/EditFormDialog";
import { useNavigate } from "react-router-dom";
import AddFormDialog from "../assets/components/AddFormDialog";

export const Homepage = ({ dataUpdate }) => {
  const navigate = useNavigate();

  const [List, setList] = useState([]);
  const [Search, setSearch] = useState("");
  const [id, setId] = useState(0);
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [hobby, setHobby] = useState("");

  const [open, setOpen] = useState(false);

  const [nameError, setNameError] = useState("");
  const [addressError, setAddressError] = useState("");
  const [hobbyError, setHobbyError] = useState("");

  // console.log(List.filter((user) => user.Name.includes("ak")));

  const addHandler = (newList) => {
    setList(newList);
    dataUpdate(newList);
  };

  const deleteHandler = (name) => {
    setList(List.filter((a) => a.Name !== name));
  };

  const editHandler = (id) => {
    let tempArr = List;
    if (!name || !name.length) {
      setNameError("nama harus diisi");
      return false;
    }
    if (!address || !address.length) {
      setAddressError("address harus diisi");
      return false;
    }
    if (!hobby || !hobby.length) {
      setHobbyError("hobby harus diisi");
      return false;
    }
    let index = tempArr.map((obj) => obj.Id).findIndex((Id) => Id === id); //map untuk mengambil objek nya saja
    tempArr[index] = {
      Name: name,
      Address: address,
      Hobby: hobby,
    };
    setList(tempArr);
    console.log(tempArr);
    setOpen(false);
  };

  const handleClickOpen = (id) => {
    setOpen(true);
    let tempObj = List.filter((a) => a.Id === id)[0];
    setId(id);
    setName(tempObj.Name);
    setAddress(tempObj.Address);
    setHobby(tempObj.Hobby);
  };

  return (
    <div>
      {/* <ButtonAppBar List={List} onListUpdate={addHandler}></ButtonAppBar> */}
      <div className="search">
        <TextField label="search" onChange={(e) => setSearch(e.target.value)}>
          search
        </TextField>
        <div className="addform">
          <AddFormDialog List={List} onListUpdate={addHandler}></AddFormDialog>
        </div>
      </div>
      {List.length !== 0 ? (
        List.filter((a) => {
          if (Search === "") {
            return a;
          } else if (a.Name.toLowerCase().includes(Search.toLowerCase())) {
            return a;
          }
          // a.Name.includes(Search);
        }).map((a) => {
          return (
            <div className="card-box" key={a.Id}>
              <div className="card-left">
                <div className="nama">{a.Name}</div>
                <div>{a.Address}</div>
              </div>
              <div className="card-right">
                <div className="hobby">{a.Hobby}</div>
                <Button
                  onClick={() =>
                    navigate("/view", {
                      state: {
                        name: a.Name,
                        address: a.Address,
                        hobby: a.Hobby,
                      },
                    })
                  }
                >
                  view
                </Button>
                <Button onClick={() => handleClickOpen(a.Id)}>edit</Button>
                <Button onClick={() => deleteHandler(a.Name)}>delete</Button>
              </div>
            </div>
          );
        })
      ) : (
        <div className="empty">0 user</div>
      )}
      <EditFormDialog
        {...{
          nameError,
          addressError,
          hobbyError,
          open,
          setOpen,
          List,
          id,
          name,
          setName,
          address,
          setAddress,
          hobby,
          setHobby,
          editHandler,
        }}
      ></EditFormDialog>
    </div>
  );
};
